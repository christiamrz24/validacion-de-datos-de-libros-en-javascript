function validarDatos(){
    letras_y_numeros_5 = /^[A-Za-z0-9_]{5,5}$/;
    letras_y_numeros_100 = /^[A-Za-z0-9_]{0,100}$/;
    letras_y_numeros_60 = /^[A-Za-z0-9_]{0,60}$/;
    letras_y_numeros_30 = /^[A-Za-z0-9_]{0,30}$/;
    codigo = document.getElementById("codigo").value;
    titulo = document.getElementById("titulo").value;
    autor = document.getElementById("autor").value;
    editorial = document.getElementById("editorial").value;
    editorial = document.getElementById("editorial").value;
    fecha_publicacion = document.getElementById("fecha-publicacion").value;
    fecha_ingreso = document.getElementById("fecha-ingreso").value;

    if(!letras_y_numeros_5.test(codigo)){
        alert("Código: Alfanumérico de 5 caracteres");
    }
    if(!letras_y_numeros_100.test(titulo)){
        alert("Título: Alfanumérico de 100 caracteres");
    }
    if(!letras_y_numeros_60.test(autor)){
        alert("Autor: Alfanumérico de 60 caracteres");
    }
    if(!letras_y_numeros_30.test(editorial)){
        alert("Editorial: Alfanumérico de 30 caracteres");
    }

    if(fecha_ingreso < fecha_publicacion){
        alert("Error: Fechas incorrectas");
    }
}